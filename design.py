#!/usr/bin/python
import gi
import sys
import json
import string

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

##loading json file


with open(sys.argv[1], 'r') as f:
    user_config = json.load(f)
    pacs1 = user_config["channels"]["clients"]["pacs1"]
    aet = pacs1.get("aet")
    ip = pacs1.get("ip")
    port = pacs1.get("port")


class Handler:
    def __init__(self,window):
        self.window=window
    def button_apply_OnButtonClicked(self,button):
        self.window.entry_aet.set_text(str(aet))
        self.window.entry_ip.set_text(str(ip))
        self.window.entry_port.set_text(str(port))
    def button_edit_OnButtonClicked(self,button):
        self.window.entry_port.set_editable(False)
        self.window.entry_aet.set_editable(False)
        self.window.entry_ip.set_editable(False)
class Window():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("design.glade")
        # self.builder.connect_signals(Handler())
        self.window = self.builder.get_object("window1")
        self.entry_aet = self.builder.get_object("entry_aet")
        self.entry_ip = self.builder.get_object("entry_ip")
        self.entry_port = self.builder.get_object("entry_port")
        self.disable_text_fields()
        self.button_apply = self.builder.get_object("button_apply")
        self.window.show_all()
        self.builder.connect_signals(Handler(self))

    def disable_text_fields(self):
        self.entry_port.set_editable(False)
        self.entry_aet.set_editable(False)
        self.entry_ip.set_editable(False)

if __name__ == "__main__":
    win = Window()
    Gtk.main()
